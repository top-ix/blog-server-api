import os
from datetime import datetime
__all__ = ['Blog']

from ming import schema as s
from ming.odm import FieldProperty, ForeignIdProperty, RelationProperty
from ming.odm.declarative import MappedClass
from apiserver.model import DBSession


class Blog(MappedClass):
    """
    Group definition.
    """
    class __mongometa__:
        session = DBSession
        name = 'blog'
        unique_indexes = [('_id',),]

    _id = FieldProperty(s.ObjectId)
    title = FieldProperty(s.String)
    body = FieldProperty(s.String)
    _user = FieldProperty(s.ObjectId)
    ispublic = FieldProperty(s.Bool, if_missing=True)
    created_at = FieldProperty(s.DateTime, if_missing=datetime.now)
