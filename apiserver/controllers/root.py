# -*- coding: utf-8 -*-
"""Main Controller"""

from tg import expose, flash, require, url, lurl
from tg import request, redirect, tmpl_context
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tg.exceptions import HTTPFound
from tg import predicates, validation_errors_response, validate, decode_params
from apiserver.model import User, DBSession, Blog
from apiserver.controllers.secure import SecureController
import re, uuid, bson
from apiserver.lib.base import BaseController
from apiserver.controllers.error import ErrorController

__all__ = ['RootController']


class ApiController(BaseController):

    @expose('json')
    @decode_params('json')
    def register(self, *args, **kw):
        try:
            email = request.POST['email']
            password = request.POST['password']
            name = request.POST['name']
            surname = request.POST['surname']
        except:
            return dict(status='Error', message='Bad request.')

        if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
            return dict(status='ERROR', message='Please insert a valid email and password.')

        if User.query.get(email_address=email):
            return dict(status='ERROR', message='Email already used, please change it.')

        token = str(uuid.uuid4())

        newuser = User(email_address=email, user_name=email, password=password, name=name, surname=surname, token=token)
        try:
            DBSession.flush()
        except:
            return dict(status="ERROR", message="Some error occures")
        return dict(status="OK", message="Welcome!", user=newuser)


    @expose('json')
    @decode_params('json')
    def login(self, *args, **kw):
        try:
            email = request.POST['email']
            password = request.POST['password']
            user = User.query.get(email_address=email)
        except:
            return dict(status='ERROR', message='Bad request.')

        if user:
            if not user.validate_password(password):
                return dict(status='ERROR', message='Wrong credentials.')
            return dict(status="OK", message="Welcome!", user=user)
        return dict(status="ERROR", message="Mail not found")


    @expose('json')
    @decode_params('json')
    def blogs(self, blogid=None, token=None, *args, **kw):
        print blogid
        print token
        print args
        print kw
        try:
            if not token:
                return dict(status="ERROR", message="Not Authorized")
            user = User.query.get(token=token)
            if not blogid:
                return dict(status="OK", blogs=Blog.query.find({"_user":user._id, "ispublic": True}).all())
            else:
                return dict(status="OK", blogs=Blog.query.get(_id=bson.ObjectId(blogid)))
        except Exception as e:
            print e
            return dict(status="ERROR", message="Wrong request")


    @expose('json')
    @decode_params('json')
    def newblog(self, token=None,*args,**kw):
        print kw
        try:
            if not token:
                return dict(status="ERROR", message="Not Authorized")
            user = User.query.get(token=token)
            title = request.POST['title']
            body = request.POST['body']
            if user:
                blog = Blog(title=title, body=body, _user=user._id)
                DBSession.flush()
                return dict(status="OK", blog=blog)
            else:
                return dict(status="ERROR", message="Not Authorized")
        except:
            return dict(status="ERROR", message="Wrong request")


    @expose('json')
    @decode_params('json')
    def updateblog(self, token=None, *args, **kw):
        try:
            blogid = request.POST['blogid']
            if not token or not blogid:
                return dict(status="ERROR", message="Bad request")
            user = User.query.get(token=token)
            blog = Blog.query.get(_id=bson.ObjectId(blogid))
            if not blog or not user:
                return dict(status="ERROR", message="Blog does not exists")
            blog.title = request.POST['title']
            blog.body = request.POST['body']
            DBSession.flush()
            return dict(status="OK", blog=blog)
        except:
            return dict(status="ERROR", message="Wrong request")


    @expose('json')
    @decode_params('json')
    def deleteblog(self, token=None, *args, **kw):
        try:
            blogid = request.POST['blogid']
            if not token or not blogid:
                return dict(status="ERROR", message="Bad request")
            user = User.query.get(token=token)
            blog = Blog.query.get(_id=bson.ObjectId(blogid))
            if not blog or not user or blog._user != user._id:
                return dict(status="ERROR", message="Wrong blog")
            blog.ispublic = False
            DBSession.flush()
            return dict(status="OK", blog=blog)
        except Exception as e:
            print e
            return dict(status="ERROR", message="Wrong request")




class RootController(BaseController):
    """
    The root controller for the apiserver application.

    All the other controllers and WSGI applications should be mounted on this
    controller. For example::

        panel = ControlPanelController()
        another_app = AnotherWSGIApplication()

    Keep in mind that WSGI applications shouldn't be mounted directly: They
    must be wrapped around with :class:`tg.controllers.WSGIAppController`.

    """
    secc = SecureController()

    error = ErrorController()

    api = ApiController()

    def _before(self, *args, **kw):
        tmpl_context.project_name = "apiserver"

    @expose('apiserver.templates.index')
    def index(self,*args, **kw):
        """Handle the front-page."""
        return dict(page='index', kw=kw)

    @expose('apiserver.templates.about')
    def about(self):
        """Handle the 'about' page."""
        return dict(page='about')

    @expose('apiserver.templates.environ')
    def environ(self):
        """This method showcases TG's access to the wsgi environment."""
        return dict(page='environ', environment=request.environ)

    @expose('apiserver.templates.data')
    @expose('json')
    def data(self, **kw):
        """
        This method showcases how you can use the same controller
        for a data page and a display page.
        """
        return dict(page='data', params=kw)

    @expose('apiserver.templates.index')
    @require(predicates.has_permission('manage', msg=l_('Only for managers')))
    def manage_permission_only(self, **kw):
        """Illustrate how a page for managers only works."""
        return dict(page='managers stuff')

    @expose('apiserver.templates.index')
    @require(predicates.is_user('editor', msg=l_('Only for the editor')))
    def editor_user_only(self, **kw):
        """Illustrate how a page exclusive for the editor works."""
        return dict(page='editor stuff')

    @expose('apiserver.templates.login')
    def login(self, came_from=lurl('/'), failure=None, login=''):
        """Start the user login."""
        if failure is not None:
            if failure == 'user-not-found':
                flash(_('User not found'), 'error')
            elif failure == 'invalid-password':
                flash(_('Invalid Password'), 'error')

        login_counter = request.environ.get('repoze.who.logins', 0)
        if failure is None and login_counter > 0:
            flash(_('Wrong credentials'), 'warning')

        return dict(page='login', login_counter=str(login_counter),
                    came_from=came_from, login=login)

    @expose()
    def post_login(self, came_from=lurl('/')):
        """
        Redirect the user to the initially requested page on successful
        authentication or redirect her back to the login page if login failed.

        """
        if not request.identity:
            login_counter = request.environ.get('repoze.who.logins', 0) + 1
            redirect('/login',
                     params=dict(came_from=came_from, __logins=login_counter))
        userid = request.identity['repoze.who.userid']
        flash(_('Welcome back, %s!') % userid)

        # Do not use tg.redirect with tg.url as it will add the mountpoint
        # of the application twice.
        return HTTPFound(location=came_from)

    @expose()
    def post_logout(self, came_from=lurl('/')):
        """
        Redirect the user to the initially requested page on logout and say
        goodbye as well.

        """
        flash(_('We hope to see you soon!'))
        return HTTPFound(location=came_from)
